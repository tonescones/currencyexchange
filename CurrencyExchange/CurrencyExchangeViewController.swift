//
//  CurrencyExchangeViewController.swift
//  CurrencyExchange
//
//  Created by Tony Li on 3/16/17.
//  Copyright © 2017 Tony Li. All rights reserved.
//

import UIKit

class CurrencyExchangeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectedCurrency: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    @IBOutlet weak var quantity: UITextField!
    
    private var adjustableRates = NSMutableArray()
    private var staticRates = NSMutableArray()
    private var multiplier = 1.0
    private let numDecimalPlaces = "0.4"
    
    override func viewWillAppear(_ animated: Bool) {
        self.selectedCurrency.text = ""
        self.date.text = ""
        self.refreshButton.action = #selector(refresh)
        self.refresh()
    }
    
    @IBAction func changeQuantity(_ sender: UITextField) {
        let quantity = Double(sender.text!)
        if quantity != nil {
            self.multiplier = quantity!
        } else {
            self.multiplier = 1.0
        }
        self.tableView.reloadData()
    }
    
    @IBAction func enterQuantity(_ sender: UITextField) {
        self.quantity.text = ""
    }
    
    @objc private func refresh() {
        CurrencyExchangeApiManager.getInstance().getCurrencyExchangeData { json in
            self.multiplier = 1.0
            self.adjustableRates.removeAllObjects()
            let rates = json["rates"]
            let baseCurrencyCountry = json["base"].string!
            self.adjustableRates.add((baseCurrencyCountry, 1.0))
            for (String: country, JSON: rate) in rates {
                self.adjustableRates.add((country, rate.doubleValue))
            }
            self.staticRates = self.adjustableRates.mutableCopy() as! NSMutableArray
            
            DispatchQueue.main.async(execute: {
                self.selectedCurrency.text = json["base"].string!
                self.date.text = json["date"].string!
                self.quantity.text = ""
                self.tableView.reloadData()
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rate = self.staticRates[indexPath.row] as! (String, Double)
        
        let selectedCountry = rate.0
        let rateRelativeToBaseCurrency = rate.1
        
        for i in 0..<self.staticRates.count {
            let rate = self.staticRates[i] as! (String, Double)
            self.adjustableRates[i] = (rate.0, rate.1 / rateRelativeToBaseCurrency)
        }
        
        self.selectedCurrency.text = selectedCountry
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.adjustableRates.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "currencyCell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "currencyCell")
        }
        
        let rate = self.adjustableRates[indexPath.row] as! (String, Double)
       
        cell!.textLabel?.text = rate.0
        cell!.detailTextLabel?.text = self.getEmojiForCurrencyCode(rate.0) + " " + String((rate.1 * self.multiplier).format(f: self.numDecimalPlaces))
        
        return cell!
    }
    
    private func getEmojiForCurrencyCode(_ currencyCode: String) -> String {
        let locale = NSLocale(localeIdentifier: currencyCode)
        return locale.displayName(forKey: NSLocale.Key.currencySymbol, value: currencyCode)!
    }
}

extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}


