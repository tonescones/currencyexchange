//
//  CurrencyExchangeApiManager.swift
//  CurrencyExchange
//
//  Created by Tony Li on 3/16/17.
//  Copyright © 2017 Tony Li. All rights reserved.
//

import Foundation

typealias ServiceResponse = (JSON, NSError?) -> Void

class CurrencyExchangeApiManager: NSObject {
    private static let _instance = CurrencyExchangeApiManager()
    
    let currencyExchangeURL = "https://api.fixer.io/latest?base=USD"
    // Test other success cases
//    let currencyExchangeURL = "https://api.fixer.io/latest?base=CAD"
    // Test failure case
//     let currencyExchangeURL = "http://api.fixer.io/latest?base=USD"
//     let currencyExchangeURL = "https://api.fixer.io/latest?base=INVALID"
    
    public static func getInstance() -> CurrencyExchangeApiManager {
        return CurrencyExchangeApiManager._instance
    }
    
    public func getCurrencyExchangeData(onCompletion: @escaping (JSON) -> Void) {
        let route = currencyExchangeURL
        getRequest(path: route, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }
    
    private func getRequest(path: String, onCompletion: @escaping ServiceResponse) {
        let request = URLRequest(url: URL(string: path)!)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            if error != nil {
                print("ERRORLOG: " + (error as? NSError).debugDescription)
                return
            }
            
            let json:JSON = JSON(data: data!)
            if json["error"] != JSON.null {
                print("ERRORLOG: Invalid URL")
                return
            }
                
            onCompletion(json, error as NSError?)
        })
        task.resume()
    }
}
